#!/bin/bash

ACTION=$1
DB_REGION=${2-us-west3}
if [ -z "${ACTION}" ]; then
    echo "an 'ACTION' is required (deploy || delete || check)"
    exit 1
fi

if [ -z "${GITLAB_TOKEN_REPO_USER}" ]; then
    echo "Environment variable 'GITLAB_TOKEN_REPO_USER' is required (typically the username of the user who issued the GitLab token)"
    exit 1
fi

if [ -z "${GITLAB_TOKEN_REPO_SYNC}" ]; then
    echo "Environment variable 'GITLAB_TOKEN_REPO_SYNC' is required (the read-repository personal token issued at GitLab>Profile>Access Tokens)"
    exit 1
fi

if [ -z "${PROJECT_ID}" ]; then
    echo "Environment variable 'PROJECT_ID' is required (Google Project ID to install resources into)"
    exit 1
fi

echo "ACTION = ${ACTION}"

# Specific to Bank of Anthos)
APPS=(accounts-db balancereader contacts frontend ledger-db ledgerwriter loadgenerator transactionhistory userservice)

# CONFIG
APPS_REPO="https://gitlab.com/multi-config-gke/bank-of-anthos.git"

# Generate a new key
function generatekey(){
    openssl genrsa -out jwtRS256.key 4096
    openssl rsa -in jwtRS256.key -outform PEM -pubout -out jwtRS256.key.pub
}

function commit_root_changes() {
    # Change to the newly created repository
    pushd ${WORKDIR}/${ROOT_REPOSITORY_NAME}
        # Commit all changes
        git status
        git add . && git commit -m '[deploy agent] updating' && git push
    popd
}

function deploy() {

    # Create RepoSync and all namepaces for root-repository
    for APP in ${APPS[@]}; do
        APP_NAMESPACE="${APP}"
        ${WORKDIR}/config-sync-setup/setup-root-repo/scripts/create-app-zone.sh ${APP_NAMESPACE} ${APPS_REPO} ${PROJECT_ID} ${DB_REGION}
    done

    # Commit changes to the root repository
    commit_root_changes

    # TODO This needs a waiting period to allow the Namespaces to be created
    #kubectl wait --for=condition=available --timeout=10s namespace/frontend
    echo "Waiting for namespaces and services to become available..." && sleep 120
    # read -p "Wait for a period of time ... hit any key to proceed"

    # Setup each namespace with publickey and git-creds to their repos
    HAS_KEY="true"
    kubectl get secret jwt-key -n "frontend" > /dev/null 2>&1
    if [[ $? != 0 ]]; then
        HAS_KEY="false"
    fi # Randomly picked to see if the key exists there

    if [[ $HAS_KEY == "false" ]]; then
        echo "Generating a new pubkey"
        generatekey
    fi

    for APP in ${APPS[@]}; do
        APP_NAMESPACE="${APP}"
        if [[ $HAS_KEY == "false" ]]; then
            echo "Creating JWT key for ${APP} namespace"
            kubectl create secret generic jwt-key -n ${APP_NAMESPACE} --from-file=./jwtRS256.key --from-file=./jwtRS256.key.pub
        fi

        kubectl delete secret git-creds -n ${APP_NAMESPACE} > /dev/null 2>&1

        echo "Creating git-creds secret for ${APP} namespace"
        kubectl create secret generic git-creds \
            --namespace="${APP_NAMESPACE}" \
            --from-literal="username=${GITLAB_TOKEN_REPO_USER}" \
            --from-literal="token=${GITLAB_TOKEN_REPO_SYNC}"
    done

    # Cleanup keys
    if [[ $HAS_KEY == "false" ]]; then
        rm -rf {jwtRS256.key,jwtRS256.key.pub} && true
    fi
}

function undeploy() {
    for APP in ${APPS[@]}; do
        APP_NAMESPACE="${APP}"
        ./scripts/remove-app-zone.sh ${APP_NAMESPACE}
    done
}

function check_dns() {
    for APP in ${APPS[@]}; do
        if [[ $APP != "loadgenerator" ]]; then
            echo "Testing DNS resolution for '${APP}'"
            kubectl exec -it dnsutils -n default -- nslookup ${APP}.${APP} 2> /dev/null
        fi
    done

}

# deploy
if [[ $ACTION == "deploy" ]]; then
    deploy
fi

# undeploy
if [[ $ACTION == "delete" ]]; then
    undeploy
fi

# deploy
if [[ $ACTION == "check" ]]; then
    check_dns
fi
