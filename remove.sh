#!/bin/bash

SUFFIX=${ITERATION_SUFFIX:-101} # MUST MATCH INSTALL FILE
curr_dir=$(pwd)
WORKDIR=$(echo $curr_dir | sed 's|\(.*\)/.*|\1|')

FANCY_OK="\e[00;48;5;40m   OK  \t\e[0m "
FANCY_CHECK="\e[00;48;5;40m   ✔   \t\e[0m "
FANCY_FAIL="\e[40;3;40;50m  FAIL \t\e[0m "
FANCY_NEUTRAL="\e[10;48;5;248m  ---  \t\e[0m "
FANCY_NONE="\e[30;48;5;248m       \t\e[0m "

if [ -f "${WORKDIR}/vars-${SUFFIX}.sh" ]; then
    source "${WORKDIR}/vars-${SUFFIX}.sh"
    echo -e "${FANCY_NEUTRAL} Using '${WORKDIR}/vars-${SUFFIX}.sh' to remove resources"
else
    echo -e "${FANCY_FAIL} Cannot find variables file produced by install. Verify ${WORKDIR}/vars-${SUFFIX}.sh"
    exit 1
fi

function remove_cluster() {
    gcloud container clusters describe "multi-repo-gke-$SUFFIX" 2>/dev/null
    if [ $? -gt 0 ]; then
        echo -e "${FANCY_NEUTRAL} Skipping removal of cluster, cluster not found"
    else
        gcloud container clusters delete "multi-repo-gke-$SUFFIX" -q  2>/dev/null
        if [ $? -gt 0 ]; then
            echo -e "${FANCY_FAIL} Failed to remove the GKE cluster. Please remove manually and try again."
        else
            echo -e "${FANCY_OK} Removed the GKE Cluster"
        fi
    fi
}

function remove_service_accounts() {
    # Remove IAM binding
    gcloud projects remove-iam-policy-binding ${PROJECT_ID} \
        --member="serviceAccount:cnrm-system@${PROJECT_ID}.iam.gserviceaccount.com" \
        --role="roles/owner"  2>/dev/null
    # Disable service account
    gcloud iam service-accounts disable cnrm-system@${PROJECT_ID}.iam.gserviceaccount.com  2>/dev/null
    if [ $? -gt 0 ]; then
        echo -e "${FANCY_FAIL} Failed to disable the cnrm-system Service Account. Please disable manually and try again."
    else
        echo -e "${FANCY_OK} Disabled the cnrm-system service account"
    fi
}

function remove_downloaded() {
    if [ -d "${CNRM_DOWNLOAD}" ]; then
        rm -rf ${CNRM_DOWNLOAD}
        if [ $? -gt 0 ]; then
            echo -e "${FANCY_FAIL} Failed to remove the download folder"
        else
            echo -e "${FANCY_OK} Removed the download folder"
        fi
    else
        echo -e "${FANCY_NEUTRAL} Skipping deletion of temp CNRM download folder"
    fi
}

function remove_root_repo() {

    gcloud source repos describe ${ROOT_REPOSITORY_NAME} 2>/dev/null
    if [ $? -gt 0 ]; then
        echo -e "${FANCY_NEUTRAL} Skipping removal of Root Repo, Repo not found"
    else
        # Remove the GSR repo
        gcloud source repos delete ${ROOT_REPOSITORY_NAME} -q
        if [ $? -gt 0 ]; then
            echo -e "${FANCY_FAIL} Failed to remove the GSR Root Repository, please remove manually"
        else
            echo -e "${FANCY_OK} Deleted the GSR Root Repository"
        fi
    fi
    # Remove local repo
    if [ -d "${WORKDIR}/${ROOT_REPOSITORY_NAME}" ]; then
        rm -rf ${WORKDIR}/${ROOT_REPOSITORY_NAME}
        if [ $? -gt 0 ]; then
            echo -e "${FANCY_FAIL} Failed to remove the local Root Repository."
        else
            echo -e "${FANCY_OK} Removed local root repository clone"
        fi
    else
        echo -e "${FANCY_NEUTRAL} Skipping deletion of local root repo"
    fi
}

function remove_variables() {
    if [ -s "${WORKDIR}/vars-${SUFFIX}.sh" ]; then
        rm -rf ${WORKDIR}/vars-${SUFFIX}.sh
        if [ $? -gt 0 ]; then
            echo -e "${FANCY_FAIL} Failed to remove variables file"
        else
            echo -e "${FANCY_OK} Removed variables file"
        fi
    else
        echo -e "${FANCY_NEUTRAL} skipping removal of variables file, file does not exist"
    fi
}

function remove_sql_databases() {
    # Forcefully remove SQL databases.
    # A better RECOMMEND appraoch is to removing SQL by commenting out or removing .yaml files from the namespace repository
    INSTANCE=$(gcloud sql instances list --format="value(name)" --project ${PROJECT_ID} 2>/dev/null)
    if [ ! -z "$INSTANCE" ]; then
        gcloud sql instances delete $INSTANCE --quiet
        if [ $? -gt 0 ]; then
            echo -e "${FANCY_FAIL} Failed to delete SQL Databases. Please remove manually and try again.\n"
            exit 1
        else
            echo -e "${FANCY_OK} Deleted SQL Database '${INSTANCE}'. NOTE: In the future, try removing the CNRM YAML files from the Bank of Anthos repository\n"
        fi
    fi
}

echo -e "${FANCY_NEUTRAL} Starting deletion"

# Start sequence
remove_cluster
remove_root_repo
remove_service_accounts
remove_downloaded
remove_root_repo
remove_variables
remove_sql_databases
