# Overview

This repository sets up a baseline infrastructure to support the overall project

# Run the demo

There are two options to run this demonstration. The first is a single script to create the demo and the corresponding script to remove resources. This is only intended for a single-use deployment within a single project. If the intention is to deploy/delete/deploy, please follow the [re-deploy steps](#re-deployment) at the bottom of this document.

## Options
1. [A simplified deployment](#1-setting-up-environment) of Bank of Anthos using Multi-Repo configuration sync.
1. For a more in-depth [workshop-style](docs/workshop.md) step-by-step, please follow [link](docs/workshop.md).

## Requirements

* Properly installed `gcloud` command-line tool configured to use the target project
* GCP Project with billing account (costs will be minimal, but may not fall completely within the free tier)

---
# 1. Setting up environment

By default, the installation script uses the context from the current `gcloud` configuration. To validate, check the following commands.

```bash
$ gcloud config configurations list

# (EXAMPLE OUTPUT)
NAME                     IS_ACTIVE  ACCOUNT           PROJECT                   COMPUTE_DEFAULT_ZONE  COMPUTE_DEFAULT_REGION
acme-company             False      person@google.com  acme-company-production   us-west3-a            us-west3
default                  False      person@google.com  my-project                us-west1-c            us-west1
edge-iot-ml-serving      False      person@google.com  edge-iot-ml-serving       us-west1-b            us-west1
multi-repo-demo          True       person@google.com  multi-repo-sync-demo      us-west3-b            us-west3
secure-cicd-pipelines    False      person@google.com  binary-authorization      us-central1-a         us-central1
```

# 2. Simple "one-click" installation

<!-- TODO: Create a new Personal Token for public use (read-only) -->
1. Obtain a [GitLab Personal Access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) and set the following environment variables:

    ```bash
    export GITLAB_TOKEN_REPO_SYNC="--- INSERT TOKEN ---"
    export GITLAB_TOKEN_REPO_USER="--- INSERT USER ---"

1. Run Setup script

    >NOTE: Grab some :coffee: or :tea:, this takes approximately 15-20 minutes to complete

    ```bash
    ./install.sh deploy
    ```

1. Interact with Bank of Anthos:

    ```bash
    export FRONTEND_ENDPOINT=$(kubectl get svc frontend -n frontend -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
    echo "Open a browser to:  http://${FRONTEND_ENDPOINT}"
    ```

1. Done!

# 3. Removing resources

1. Removing resources used in this demo can be completed using the `remove` script

    ```bash
    ./remove.sh
    ```

    >NOTE: Due to restrictions on naming of databases, running this script will remove the SQL Database and SQL Instance triggering a 7-day name re-use. If deploying, removing, deploying again, please refer to the [deployment options](#deployment-options) below.

# 4. Further Testing (Optional)

If you plan to use ACM/Config-Sync beyond just the demo, you will need to control the `namespace` repositories. Currently, the base repository for Bank of Anthos is configured to point at a slightly-modified BOA within the same GitLab Subgroup as this repo. It is recommended to fork the opinionated Bank of Anthos deployment (see next section).
## Opinionated Fork Bank of Anthos
1. Fork the [Opinionated Bank of Anthos](https://gitlab.com/multi-config-gke/bank-of-anthos.git) repo
1. Create a [GitLab Personal Access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) for the forked repo (optional, previous access token may be sufficient)
1. Add an Environment Variable (`BOA_FORKED_REPO`) that points to the forked repo's URL
    ```bash
    # Example
    export BOA_FORKED_REPO="https://gitlab.com/my-user-forked-repo/bank-of-anthos.git"
    ```

### Changes to Bank of Anthos

The Bank of Anthos is a direct [clone of the upstream](https://github.com/GoogleCloudPlatform/bank-of-anthos), but with an additional folder added `/cluster-config` where the contents of the `/kubernetes-manifests` have been setup to put each service in their own namespace. For example, `/kubernetes-manifests/ledgerwriter.yaml` has been copied to `/cluster-config/ledgerwriter/ledgerwriter.yaml` along with any configuration changes needed to run Bank of Anthos in a multi-namespace configuration.

## Re-deployment
If the use of this demo requires you to deploy, remove, deploy, remove, you will notice a few issues related to underlying policies in GCP. Primarily, the challenges are related to deleting and re-using previously created resources like Service Accounts and SQL Databases. In general, the `install.sh` and `remove.sh` script handles these with the exception of the *ConfigConnector* resources in the Bank of Anthos `cluster-config/accounts-db` folder.

    1. If starting from a running demo instance, execute `remove.sh` from this repository
        >WARNING: This removes all resources
    1. Inside of the forked repo, change references to `accounts-db-instance-XXX` in `/cluster-config/accounts-db/account-db-cnrm.yaml` where XXX is a unique suffix.
    1. Save, commit and push
    1. Inside this repository, update the `setup-root-repo/app-zone-template/accounts-db-cnrm-config-secret.yaml.tpl` line #18 with the new name/suffix
    1. Re-run `install.sh`
    1. NOTE: This will need to be performed for every re-deployment (if you want to help contribute a script to perform both repo changes, please submit a PR)

