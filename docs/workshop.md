# Overview

This workshop goes through a step-by-step process in building a real-world use case showcasing a "shift-left" scenario.

# Stages
1. Setting up a GCP project & understanding primary principles
1. Provisioning a GKE cluster ready for [Workload Identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity) and [Config Connector (CNRM)](https://cloud.google.com/config-connector)
1. Configuring namespace-based Workload Identity
1. Configure namespace-based CNRM
1. Setup [Multi-Repo configuration](https://cloud.google.com/kubernetes-engine/docs/add-on/config-sync/how-to/multi-repo) capabilities

# Concepts
* Shift-Left
* GitOps sync
* Workload Identity
* Config Connector (CNRM)
* Multi-repo sync
* Anthos Configuration Management (ACM) vs Config Sync

# Manual Workshop Installation

## 0. Setup a GCP project

1. Setup `gcloud` CLI tools for the following properties:

    ```bash
    export PROJECT_ID="<INSERT PROJECT ID HERE>"
    export REGION="<INSERT PREFERRED REGION>"
    export ZONE="<INSERT PREFERRED ZONE WITHIN REGION>"

    gcloud config set project ${PROJECT_ID}
    gcloud config set compute/region ${REGION}
    gcloud config set compute/zone "${ZONE}"
    ```


## 1. Setup Baseline Cluster

1. Enable API services within GCP

    ```bash
    gcloud services enable \
        container.googleapis.com \
        containerregistry.googleapis.com \
        cloudresourcemanager.googleapis.com \
        sqladmin.googleapis.com \
        sourcerepo.googleapis.com
    ````

    >NOTE: This demo does not require or directly use Anthos, however, Anthos does offer some features like HierarchyControllers and Policy Management. If you would like to test those out using this lab, a supplemental section on "[Supplemental Anthos config management](#supplemental-anthos-config-management)" is provided and can be run after the workshop.

1. Setup shell variables used throughout the workshop.

    ```bash
    # Working directory for demo
    export WORKDIR=$(echo $(pwd) | sed 's|\(.*\)/.*|\1|') # Folder above this repo is our base
    # GCP Project associated with the current gcloud authenticated user
    export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
    # GCP Project number derived from the PROJECT_ID
    export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
    # GCP Region to deploy resources into
    export REGION=$(gcloud config get-value compute/region 2> /dev/null)
    # GCP Zone within the Region to deploy the cluster into
    export ZONE=$(gcloud config get-value compute/zone 2> /dev/null)
    # Current gcloud user's email address (used for authentication to Source Repositories)
    export ACCOUNT_EMAIL=$(gcloud auth list --filter=status:ACTIVE --format="value(account)")
    # Root configuration's repository name
    export ROOT_REPOSITORY_NAME="root-repo"
    # Use Anthos subscription products (adds PolicyManager)
    export USE_ANTHOS="false"
    # Define your cluster suffix (ie: 001)
    export SUFFIX="YOUR_SUFFIX"
    # GitLab Private Token for Git Sync access to repository (note only Read-Only access is required)
    export GITLAB_TOKEN_REPO_SYNC="__INSERT_GITLAB_TOKEN__"
    export GITLAB_TOKEN_REPO_USER="__INSERT_GITLAB_USERNAME__"

    echo "Variable values being used, please double check for validity:"
    echo "PROJECT_ID=${PROJECT_ID}"
    echo "REGION=${REGION}"
    echo "ZONE=${ZONE}"
    echo "ACCOUNT_EMAIL=${ACCOUNT_EMAIL}"
    echo "GITLAB_TOKEN_REPO_SYNC=${GITLAB_TOKEN_REPO_SYNC}"
    echo "GITLAB_TOKEN_REPO_USER=${GITLAB_TOKEN_REPO_USER}"
    ```

1. Create a GKE cluster

    Create a cluster that is enabled for workload identity and will be configured for namespace-based workload identity later in this workshop.

    >NOTE: The created cluster will use `preemptible` VMs to reduce costs for demo purposes.

    ```bash
    gcloud container clusters create "multi-repo-gke-$SUFFIX" \
        --release-channel rapid \
        --preemptible --enable-ip-alias \
        --workload-pool=${PROJECT_ID}.svc.id.goog \
        --enable-autoscaling --min-nodes 1 --max-nodes 4 --num-nodes 2 \
        --machine-type=n1-standard-4 \
        --labels mesh_id="proj-${PROJECT_NUMBER}" \
        --scopes=cloud-platform \
        --quiet \
        --zone ${ZONE}

    gcloud container clusters get-credentials cluster-${SUFFIX} --zone ${ZONE} --project ${PROJECT_ID}

    kubectl cluster-info
    ```

1. Install Config Connector to use KRM-based files to deploy GCP resources. First, create a Google Service Account (GSA)

    ```bash
    gcloud iam service-accounts create cnrm-system
    ```

1. Give the CNRM service account permissions to create GCP resources

    >NOTE: Take careful consideration on the permissions granted to the service account to ensure the minimal level of permissions are given based on the needs of the project and service.

    ```bash
      # Give permissions to the new service account
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
        --member="serviceAccount:cnrm-system@${PROJECT_ID}.iam.gserviceaccount.com" \
        --role="roles/editor"
    ```

1. Install Configuration Management Operator.
    >NOTE: Optionally, if using Anthos, apply `01-config-management-operator-anthos.yaml` instead of `01-config-sync-operator-non-anthos.yaml`. See [Supplemental Anthos Configuration options](#supplemental-anthos-configuration-options)

    ```bash
    kubectl apply -f ${WORKDIR}/baseline-infrastructure/01-config-sync-operator-non-anthos.yaml

    kubectl wait --for=condition=available --timeout=600s deployment config-management-operator -n kube-system
    ```

1. Create a `root` repository in Google Source Repositories

    ```bash
    export ROOT_REPO_DIR="${WORKDIR}/${ROOT_REPOSITORY_NAME}"

    # Create Google Source Repo
    gcloud source repos create ${ROOT_REPOSITORY_NAME}

    # Clone newly created repo
    gcloud source repos clone ${ROOT_REPOSITORY_NAME} ${ROOT_REPO_DIR} --dry-run

    # Copy template repo into folder
    cp -r setup-root-repo/cluster-cfg ${ROOT_REPO_DIR}/

    ```

    >NOTE: Committing changes to the repository MAY require you to configure git with `user.name` and `user.email`. As an example, you can use:

    ```bash
    # Optional git repo config
    git config --local user.email "$ACCOUNT_EMAIL"
    git config --local user.name "GCP Demo"

    # Commit and push changes
    cd ${ROOT_REPO_DIR}
    git add . && git commit -m '[setup agent] Initial install' && git push
    ```

1. Install Config-Sync operator with initial `RootSync` definition
    >NOTE: This is step is installing the non-Anthos version of ConfigSync, substitute `02-config-sync-config-anthos.yaml` if the project is using Anthos. See [Supplemental Anthos Configuration options](#supplemental-anthos-configuration-options)

    ```bash
    export CONFIG_REPO="https://source.developers.google.com/p/${PROJECT_ID}/r/${ROOT_REPOSITORY_NAME}"
    # Replace variable for root-sync's repository and apply to GKE
    kubectl apply -f ${WORKDIR}/config-sync-setup/02-config-sync-config-config-sync.yaml
    # Wait for deployment to complete
    kubectl wait --for=condition=established --timeout=60s crd configmanagements.configmanagement.gke.io
    ```

1. Verify RootSync configuration statuses

    ```bash
    kubectl wait --for=condition=established --timeout=60s crd rootsyncs.configsync.gke.io
    ```

1. Configure `RootSync` operator

    ```bash
    CONFIG_REPO="https://source.developers.google.com/p/${PROJECT_ID}/r/${ROOT_REPOSITORY_NAME}"
    # Replace RootSync's repository variable with newly created root-repo and apply
    sed "s|%%REPO%%|${CONFIG_REPO}|g" ${WORKDIR}/config-sync-setup/03-root-sync-config.yaml.tpl | kubectl apply -f -
    echo "Waitin' for RootSync to become available" && sleep 30
    # Enable Workload Identity
    gcloud iam service-accounts add-iam-policy-binding \
        --role roles/iam.workloadIdentityUser \
        --member "serviceAccount:${PROJECT_ID}.svc.id.goog[config-management-system/root-reconciler]" \
        ${PROJECT_NUMBER}-compute@developer.gserviceaccount.com
    # Link KSA to GSA
    kubectl annotate serviceaccount -n config-management-system root-reconciler \
        iam.gke.io/gcp-service-account=${PROJECT_NUMBER}-compute@developer.gserviceaccount.com

    kubectl wait --for=condition=available --timeout=600s deployment root-reconciler -n config-management-system
    ```

1. Download and install Config Connector

    ```bash
    mkdir -p ${CNRM_DOWNLOAD}
    # Download latest bundle of Controller & Operator YAML files
    gsutil cp gs://configconnector-operator/latest/release-bundle.tar.gz ${CNRM_DOWNLOAD}/release-bundle.tar.gz
    tar zxvf ${CNRM_DOWNLOAD}/release-bundle.tar.gz -C ${CNRM_DOWNLOAD}

    # Setup within the cluster
    kubectl apply -f ${CNRM_DOWNLOAD}/operator-system/configconnector-operator.yaml
    # Wait for all pods to become ready
    kubectl wait -n configconnector-operator-system --for=condition=Ready pod --all

    # Configure the ConfigConnector controller
    kubectl apply -f ${WORKDIR}/config-sync-setup/04-config-connector.yaml

    ```

1. List all available Config Connector resources (optional)

    ```bash
    kubectl get crds --selector cnrm.cloud.google.com/managed-by-kcc=true
    ```

## 2. Install Bank of Anthos

In this section, we will install Bank of Anthos from a slightly modified public repository. The source code and structure of the public repository is the same, the only addition is the `/cluster-config` folder where the original `/kubernetes-manifests` have been re-positioned inside of folders corresponding to Namespaces. The public Bank-of-Anthos repository is designed to deploy into one namespace. Anthos Config Management/Config-Sync is designed to deploy services into their own namespaces to allow for granular security, traffic management and self-contained GCP resource creation.

>NOTE: The only changes to the `/kubernetes-manifests` files were to the `ConfigMap` files with details on Service endpoint URIs to include the `namespace.service` pattern, and the `accounts-db` service to build a CloudSQL database (`SQLDatabase` and `SQLInstance` instead of a containerized version of Postgres)

1. Setup the environment variables for the Landing Zones. Each namespace represents a microservice (ie, balancereader, frontend, contacts, etc). Each namespace has a corresponding git repository where the YAML manifests and configuration exists for that service.
    >NOTE: For simplicity, the ConfigSync objects are setup to use the same Bank-of-Anthos git repository, but use a different `dir` variable defined in the `RepoSync` objects to correspond to it's namespace folder.

    ```bash
    export GITLAB_TOKEN_REPO_USER="<insert gitlab username>"
    export GITLAB_TOKEN_REPO_SYNC="<insert personal read-only repository token>"
    ```

1. For simplicity, a helper script has been provided to loop through each of the services to create a Landing Zone for each service.

    ```bash
    ${WORKDIR}/config-sync-setup/setup-root-repo/bank-of-anthos-app-zone.sh deploy
    ```

1. Verify BoA deployment

    >NOTE: The deployment of Bank of Anthos is due to the feature functionality within Config Sync. All YAML files contained within the `/cluster-config` + `/[namespace]` are deployed to their respective namespaces due to the configuration of `RootSync` ([link](#))

    Choosing two of the lengthier deployments in the Bank of Anthos, `frontend` and `accounts-db`, we can wait on their status
    ```bash
    # Ledger DB (StatefulSet, checking the pods that are created)
    kubectl wait --for=condition=ready pod --timeout=60s -l statefulset.kubernetes.io/pod-name=ledger-db-0 -n ledger-db
    # frontend service is required for obtaining the IP of the LoadBalancer
    kubectl wait --for=condition=available --timeout=600s deployment frontend -n frontend
    ```

1. Get the IP address of the LoadBalancer created in the `frontend` service deployment

    ```bash
    FRONTEND_ENDPOINT=$(kubectl get svc frontend -n frontend -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
    echo "Open a browser to:  http://${FRONTEND_ENDPOINT}"
    ```

1. done!


# Supplemental Anthos config management

1. Enable Anthos services before running the installation of the Operator

    ```bash
    gcloud services enable anthos.googleapis.com
    ```

# Root & Application repository structures

![structure](multi-repo-structure-sequence.png)

1. Root-configuration repository containing the overall clusters configuration. Typically maintained by an Operations or DevOps Team.

1. Folder within the `root-configuration` repository containing the kubernetes configuration including application landing zones (ie, namespaces with base configuration).

1. Application landing zone for "app-1" inside the `root-configuration` repository containing configuration to load `RepoSync` objects from an external git repository. In this scenario, the remote git repository is the "app-1" source repository. The configuration is set to read configs from the `/cluster-config` folder within the application's source repository.

1. Application's source repository containing application code and Kubernetes configuration files (ie: Service, Deployment, ConfigMaps, etc) under the `/cluster-config` folder. Configurations contained within this folder will be `kubectl apply` inside the designated by the Namespace `root-configuration` repository (step #3)

# Known issues

* `nomos` is not working with the status

# Troubleshooting

## Workload Identity

Workload identity authorizations from within the namespaces use the k8s `ServiceAccount` using the pattern `[NAMESPACE]-ksa` and correspond to the GSA `[NAMESPACE]@[PROJECT_ID].iam.gserviceaccount.com`

1. Verify that the CNRM GSA and KSA are coordinating
    ```bash
    # Test workload identity
    kubectl run -it \
    --image google/cloud-sdk:slim \
    --serviceaccount ${NAMESPACE}-ksa \
    --namespace ${NAMESPACE} \
    workload-identity-test
    ```

1. Within the new prompt, list the `gcloud auth`
    ```bash
    gcloud auth list
    ```

1. Test creating a new SQL instance
    ```bash
    gcloud container clusters list
    ```

1. Verify that the GKE cluster is the one associated with this project

## CNRM

CNRM when setup as "namespace" uses the `cnrm-system` namespace to contain the k8s `ServiceAccount` under the pattern `cnrm-controller-manager-[NAMESPACE]`.

1. Verify that the CNRM GSA and KSA are coordinating
    ```bash
    # Test workload identity
    kubectl run -it \
    --image google/cloud-sdk:slim \
    --serviceaccount cnrm-controller-manager-${NAMESPACE} \
    --namespace cnrm-system \
    workload-identity-test
    ```

1. Within the new prompt, list the `gcloud auth`
    ```bash
    gcloud auth list
    ```

1. Test creating a new SQL instance
    ```bash
    gcloud sql instances create mysql-instance \
    --database-version=MYSQL_5_7 \
    --region=us-central1 \
    --cpu=2 \
    --memory=4G \
    --root-password="password"
    ```
