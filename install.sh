#!/bin/bash

# optional debug that leaves most state and outputs more logs
DEBUG="${DEBUG:-false}"
SUFFIX=${ITERATION_SUFFIX:-101} # Script local - 3-digit suffix
USE_ANTHOS=${USE_ANTHOS_USER:-"false"}
INSTALL_TEKTON=${INSTALL_TEKTON:-"false"}
# Used in 'bank-of-anthos-app-zone.sh' to define where each service's configuration exists
# Replace this value IF forked from this original repo OR with explicit understanding of
#    repository structure for multi-namespace Bank-of-Anthos
# Override by either changing this variable or defining an environment variable "BOA_FORKED_REPO"
export APPS_REPO=${BOA_FORKED_REPO:-"https://gitlab.com/multi-config-gke/bank-of-anthos.git"}

##### INTERNAL VARIABLES

FANCY_OK="\e[00;48;5;40m   OK  \t\e[0m "
FANCY_CHECK="\e[00;48;5;40m   ✔   \t\e[0m "
FANCY_FAIL="\e[40;3;40;50m  FAIL \t\e[0m "
FANCY_NEUTRAL="\e[10;48;5;248m  ---  \t\e[0m "
FANCY_NONE="\e[30;48;5;248m       \t\e[0m "
WORKDIR=$(echo $(pwd) | sed 's|\(.*\)/.*|\1|')

ACTION=$1
if [ -z "${ACTION}" ]; then
    echo -e "${FANCY_NEUTRAL} An 'ACTION' can be passed as the first argument to deploy or check installation (deploy || check)"
    ACTION="deploy" # Default to "deploy"
fi

# If vars file already exists, run source
if [ -f "${WORKDIR}/vars-${SUFFIX}.sh" ]; then
  source "${WORKDIR}/vars-${SUFFIX}.sh"
else
  echo -e "${FANCY_OK} Creating vars file"

  PROJECT_ID=$(gcloud config list --format 'value(core.project)')
  cat <<EOT > ${WORKDIR}/vars-${SUFFIX}.sh
export WORKDIR=${WORKDIR}
export SUFFIX=${SUFFIX}
export PROJECT_ID=${PROJECT_ID}
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export REGION=$(gcloud config get-value compute/region 2> /dev/null)
export ZONE=$(gcloud config get-value compute/zone 2> /dev/null)
export ACCOUNT_EMAIL=$(gcloud auth list --filter=status:ACTIVE --format="value(account)")
export ROOT_REPOSITORY_NAME="root-repo-${SUFFIX}"
export USE_ANTHOS="${USE_ANTHOS}"
export CNRM_DOWNLOAD=${WORKDIR}/config-sync-setup/cnrm-download
export GITLAB_TOKEN_REPO_SYNC="${GITLAB_TOKEN_REPO_SYNC}"
export GITLAB_TOKEN_REPO_USER="${GITLAB_TOKEN_REPO_USER}"
export INSTALL_TEKTON="${INSTALL_TEKTON}"
EOT

  source ${WORKDIR}/vars-${SUFFIX}.sh

fi # end if vars file exists

if [ "${DEBUG}" == "true" ]; then
  echo -e "\n${FANCY_CHECK}Using: '$WORKDIR' as the base directory\n"
  echo -e "${FANCY_NEUTRAL}Environment Variables\n=====================\n"
  cat ${WORKDIR}/vars-${SUFFIX}.sh
  echo ""
fi

# Make sure gitlab variables are defined
if [ -z "${GITLAB_TOKEN_REPO_USER}" ]; then
    echo -e "${FANCY_FAIL} Environment variable 'GITLAB_TOKEN_REPO_USER' is required, please set"
    exit 1
fi

if [ -z "${GITLAB_TOKEN_REPO_SYNC}" ]; then
    echo -e "${FANCY_FAIL} Environment variable 'GITLAB_TOKEN_REPO_SYNC' is required, please set"
    echo -en "\nRefer to https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token to create a token\n\nToken should only be read-only repository\n"
    exit 1
fi

# Setup projct environment
function setup_gcp() {
  echo "Enabling required API services"
  gcloud services enable \
      container.googleapis.com \
      containerregistry.googleapis.com \
      cloudresourcemanager.googleapis.com \
      sqladmin.googleapis.com \
      sourcerepo.googleapis.com

  if [ "${USE_ANTHOS}" == "true" ]; then
    gcloud services enable anthos.googleapis.com
  fi
}

# Create cluster
function create_cluster() {
  local CLUSTER_EXISTS=$(gcloud container clusters describe "cluster-${SUFFIX}" --format="value(name)" 2> /dev/null)
  if [ -z "${CLUSTER_EXISTS}" ]; then
    # Cluster does not exist
    INIT_NODES=2
    echo "Creating a new GKE Cluster...(may take 3-10 minutes)"
    if [[ "${INSTALL_TEKTON}" == "true" ]]; then #Setup an extra node for Tekton
      INIT_NODES=3
    fi

    gcloud container clusters create "multi-repo-gke-$SUFFIX" \
        --release-channel rapid \
        --preemptible --enable-ip-alias \
        --workload-pool=${PROJECT_ID}.svc.id.goog \
        --enable-autoscaling --min-nodes 1 --max-nodes 6 --num-nodes ${INIT_NODES} \
        --machine-type=n1-standard-4 \
        --labels mesh_id="proj-${PROJECT_NUMBER}" \
        --scopes=cloud-platform \
        --quiet \
        --zone ${ZONE}
    echo -e "${FANCY_OK} Cluster created, use this command to create your 'kubectl' context configuration:"
  else
    echo -e "${FANCY_NEUTRAL} Cluster already exists, moving on"
  fi

  echo "Re-connect to cluster:  gcloud container clusters get-credentials cluster-${SUFFIX} --zone ${ZONE} --project ${PROJECT_ID}"
  gcloud container clusters get-credentials cluster-${SUFFIX} --zone ${ZONE} --project ${PROJECT_ID}
}

# Setup GCP for config connector
function create_service_account {
  GSA_NAME=$1
  GSA_ROLE=$2
  local CURR_SA_NAME=$(gcloud iam service-accounts describe ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com --format="value(name)" 2> /dev/null)
  if [ -z "$CURR_SA_NAME" ]; then
    # Create a service account that can create resources for this project
    gcloud iam service-accounts create ${GSA_NAME}
  else
    gcloud iam service-accounts enable "${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"
  fi

  # Give permissions to the new service account
  gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="${GSA_ROLE}"
}

function install_tekton_resources {

  echo -e "${FANCY_OK} Installing Tekton"

  TEKTON_NAMESPACE="tekton-pipelines"

  # Install Operator
  kubectl apply -f https://storage.googleapis.com/tekton-releases/operator/previous/v0.21.0-1/release.yaml
  kubectl wait --for=condition=available --timeout=600s --namespace tekton-operator deployment tekton-operator

  # Install configuration (All tekton components)
  kubectl apply -f https://raw.githubusercontent.com/tektoncd/operator/main/config/crs/kubernetes/config/all/operator_v1alpha1_config_cr.yaml
  kubectl wait --for=condition=available --timeout=600s --namespace ${TEKTON_NAMESPACE} deployment tekton-pipelines-controller

  echo -e "${FANCY_OK} Installing Tekton Config and Secrets"

  local GSA_NAME="tekton-builder"
  TEKTON_BUCKET="${PROJECT_ID}-tekton-resources"
  # Create a tekton agent to run builds
  create_service_account "${GSA_NAME}" "roles/editor"

  echo -e "${FANCY_OK} Creating Tekton resources bucket ${TEKTON_BUCKET}"

  # Make bucket if not exists
  gsutil ls -p $PROJECT_ID gs://${TEKTON_BUCKET} &> /dev/null
  if [ $? -gt 0 ]; then
    gsutil mb -p $PROJECT_ID gs://${TEKTON_BUCKET}
  fi

  if [ ! -f ./gsa-key.json ]; then
      echo "Keyfile does not exist, create a new one"
      gcloud iam service-accounts keys create ./gsa-key.json --iam-account="${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"
  fi

  # create secret
  if [ -f ./gsa-key.json ]; then
      kubectl create secret generic tekton-storage -n ${TEKTON_NAMESPACE} --from-file=gcs-config=./gsa-key.json
  fi

  # create configmap
  kubectl create configmap config-artifact-bucket -n ${TEKTON_NAMESPACE} \
      --from-literal=location=gs://${TEKTON_BUCKET} \
      --from-literal=bucket.service.account.secret.name=tekton-storage \
      --from-literal=bucket.service.account.secret.key=gcs-config \
      --from-literal=bucket.service.account.field.name=GOOGLE_APPLICATION_CREDENTIALS

}

# Install Config Connector into the cluster
function install_config_connector {

  # Using "Namespaced" version
  mkdir -p ${CNRM_DOWNLOAD}

  gsutil cp gs://configconnector-operator/latest/release-bundle.tar.gz ${CNRM_DOWNLOAD}/release-bundle.tar.gz
  tar zxvf ${CNRM_DOWNLOAD}/release-bundle.tar.gz -C ${CNRM_DOWNLOAD}

  kubectl apply -f ${CNRM_DOWNLOAD}/operator-system/configconnector-operator.yaml
  # Wait for all pods to become ready
  kubectl wait -n configconnector-operator-system --for=condition=Ready pod --all

  # install config connector controller
  kubectl apply -f ${WORKDIR}/config-sync-setup/04-config-connector.yaml

  if [ $? -gt 0 ]; then
    echo -e "${FANCY_FAIL} Problems installing cnrm / Config Connector"
    exit 1
  fi

}

function setup_acm() {
  ## Setup user for Cloud Source Repositories
  gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member serviceAccount:${PROJECT_NUMBER}-compute@developer.gserviceaccount.com \
    --role roles/source.reader

  # Install Operator
  if [ "${USE_ANTHOS}" == "true" ]; then
    source ${WORKDIR}/config-sync-setup/01-config-management-operator-anthos.sh
  else
    source ${WORKDIR}/config-sync-setup/01-config-sync-operator.sh
  fi
  # Wait for operator deployment to complete
  kubectl wait --for=condition=available --timeout=600s deployment config-management-operator -n kube-system
  # Configure Operator

  if [ "${USE_ANTHOS}" == "true" ]; then
    kubectl apply -f ${WORKDIR}/config-sync-setup/02-config-sync-config-anthos.yaml
  else
    kubectl apply -f ${WORKDIR}/config-sync-setup/02-config-sync-config-config-sync.yaml
  fi

  # Wait for ConfigManagement & RootSync CRDs
  kubectl wait --for=condition=established --timeout=60s crd configmanagements.configmanagement.gke.io
  sleep 30 # apparently it's very shakey on how soon RootSync is ready
  kubectl wait --for=condition=established --timeout=60s crd rootsyncs.configsync.gke.io
}

# Setup "RootSync" object within k8s using newly created root-repository
function configure_root_configmanagement() {
  local CONFIG_REPO="https://source.developers.google.com/p/${PROJECT_ID}/r/${ROOT_REPOSITORY_NAME}"
  # Replace RootSync's repository variable with newly created root-repo and apply
  sed "s|%%REPO%%|${CONFIG_REPO}|g" ${WORKDIR}/config-sync-setup/03-root-sync-config.yaml.tpl | kubectl apply -f -
  echo "Waitin for RootSync to become available" && sleep 30 # TODO: avoid a static wait ...waiting on root-sync to be available
  # Enable Workload Identity
  gcloud iam service-accounts add-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[config-management-system/root-reconciler]" \
    ${PROJECT_NUMBER}-compute@developer.gserviceaccount.com
  # Link KSA to GSA
  kubectl annotate serviceaccount -n config-management-system root-reconciler \
    iam.gke.io/gcp-service-account=${PROJECT_NUMBER}-compute@developer.gserviceaccount.com

  kubectl wait --for=condition=available --timeout=600s deployment root-reconciler  -n config-management-system
  if [ $? -gt 0 ]; then
    echo -e "${FANCY_FAIL} Problems setting up RootSync configuration"
    exit 1
  fi
}

# Setup root configuration repository
function create_root_repository() {

  echo "Creating Root Repository"

  local ROOT_REPO_DIR="${WORKDIR}/${ROOT_REPOSITORY_NAME}"

  # Create Google Source Repo
  gcloud source repos create ${ROOT_REPOSITORY_NAME}

  # Clone newly created repo
  gcloud source repos clone ${ROOT_REPOSITORY_NAME} ${ROOT_REPO_DIR}

  # Copy template repo into folder
  cp -r setup-root-repo/cluster-cfg ${ROOT_REPO_DIR}/

  # Commit and push changes
  cd ${ROOT_REPO_DIR}
  # Configure local repo's user/pass
  git config --local user.email "$ACCOUNT_EMAIL"
  git config --local user.name "GCP Demo"

  # Commit changes
  git add . && git commit -m '[setup agent] Initial install' && git push
  if [ $? -gt 0 ]; then
    echo -e "${FANCY_FAIL} Failed to push initial Root Repository"
    exit 1
  fi
}

####################################################################
####################################################################
####################################################################

if [ "${ACTION}" ==  "delete" ]; then
  source ./remove.sh
elif [ "${ACTION}" == "tekton" ]; then
  install_tekton_resources
elif [ "${ACTION}" ==  "check" ]; then
  ${WORKDIR}/config-sync-setup/setup-root-repo/bank-of-anthos-app-zone.sh check
  echo -e "${FANCY_OK} Grabbing endpoint...."
  echo -e "${FANCY_OK} =============================="
  FRONTEND_ENDPOINT=$(kubectl get svc frontend -n frontend -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
  echo -e "${FANCY_OK} Go to your frontend at: http://${FRONTEND_ENDPOINT}"
  echo -e "${FANCY_OK} Default user: testuser"
  echo -e "${FANCY_OK} Default pass: password"
  echo ""
elif [ "${ACTION}" ==  "deploy" ]; then
  ###          ###
  ### Sequence ###
  ###          ###
  # TODO Reenable this and/or put guards around for idempotency
  # 1. Setup GCP
  setup_gcp

  # 1.1 setup cnrm-system (config-connector)
  create_service_account "cnrm-system" "roles/owner"

  # 2. Create a cluster
  create_cluster

  # 2.1 Enable configmanagement
  gcloud alpha container hub config-management enable || true # skip fail if alpha isn't installed

  # 3. Create Root Repo to contain operator's clusters-wide configuration
  create_root_repository

  # 4. Install & Setup ACM
  setup_acm

  # 4.1 Install cnrm-system
  install_config_connector

  # 5. Setup ConfigManagement operatorlanding
  configure_root_configmanagement

  # 6. Create Namespaces for each application (by running script to deploy all namespaces)
  ${WORKDIR}/config-sync-setup/setup-root-repo/bank-of-anthos-app-zone.sh deploy

  # Two rather lengthy deployments, when these are both ready, the whole system should be ready
  kubectl wait --for=condition=ready pod --timeout=60s -l statefulset.kubernetes.io/pod-name=accounts-db-0 -n accounts-db
  # frontend service is required for obtaining the IP of the LoadBalancer
  kubectl wait --for=condition=available --timeout=600s deployment frontend -n frontend

  # 7. Verify finshed
  # TODO Add a `kubectl wait` for the loadbalanacer instead of sleeping
  echo "Waiting for frontend load balancer to become available"
  sleep 60s
  ${WORKDIR}/config-sync-setup/setup-root-repo/bank-of-anthos-app-zone.sh check

  # 8. obtain frontend IP address from load balancer
  FRONTEND_ENDPOINT=$(kubectl get svc frontend -n frontend -o jsonpath='{.status.loadBalancer.ingress[0].ip}')

  echo "Go to your frontend at: http://${FRONTEND_ENDPOINT}"
  echo "Default user: testuser"
  echo "Default pass: password"
  echo ""

  # 9. Optionally, install tekton
  if [ "${INSTALL_TEKTON}" == "true" ]; then
    install_tekton_resources
  fi

  # TODO: Perhaps keep for dynamically adding more namespaces later
  if [ "${DEBUG}" == "false" ]; then
    remove_downloaded
  fi
fi