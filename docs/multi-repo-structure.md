# Logical Repository Overview

```mermaid
graph LR
    start --> S
    subgraph 1. Install Operator
        S{Using Anthos?} -- Yes --> ANTHOS(Anthos Operator)
        S -- No --> CONFIG_SYNC(GKE Config-Sync)
    end

    subgraph 2. Setup RootSync
        ANTHOS --> ROOT_REPO[(Root Repo)]
        CONFIG_SYNC --> ROOT_REPO[(Root Repo)]
    end

    subgraph 3. App Config
        ROOT_REPO -- RepoSync --> APP_1[(App 1 Repo)]
        ROOT_REPO -- RepoSync --> APP_2[(App 2 Repo)]
        ROOT_REPO -- RepoSync --> APP_3[(App 3 Repo)]
    end

```

# Repository Structure

![structure](multi-repo-structure-sequence.png)

1. Root configuration repository containing multi-cluster gitops configuration state and supporting scripts
1. Subfolder containing configuration representing desired state
1. `RepoSync` objects containing a reference to a git repository for an application's configuration
1. Application repository with convention-based subfolder `/cluster-config` containing Kubernetes manifests for the project
1. Additional application repository

## Setting it up

1. `cluster-cfg` is a subfolder of the `root-configuration` git repository.

    The `root-configuration` repository can be crated with the following steps:

    1. Create an empty folder
        ```bash
        mkdir root-configuration
        cd root-configuration
        ```

    1. Download the `nomos` CLI -- [Nomos Installation](https://cloud.google.com/anthos-config-management/docs/how-to/nomos-command#installing)
        >NOTE: Nomos is not required, but recommended to ensure proper repository structure

    1. Create a folder within the `root-configuration` to contain the contents of the Root Sync configuration

        ```bash
        mkdir cluster-cfg
        cd cluster-cfg
        # NOTE: This will be the "dir" containing the operational configuration for the entire multi-cluster
        ```

    1. Create the repository structure with `nomos`
        ```bash
        nomos init
        ```

1. Each Application consisting of 1 or more highly cohesive services will be deployed into a Kubernetes `namespace`, based on the folder structure in `cluster-cfg/namespaces`. In this demo, there is a 1:1 application to namespace configuration.

1. This demonstration implements best practices by including a `namespace inheritance` where contents of the folder are deployed into every subfolder namespace. This is described in the [official namespace inheritance documentation](https://cloud.google.com/anthos-config-management/docs/concepts/namespace-inheritance). This is demonstrated with `limits.yaml` and `service-account.yaml` in the [Repository Structure image](#repository-structure) in the `/cluster-cfg/namespaces/app-group-1/` folder. Both of these files will be applied to `app-1` and `app-2`.

1. Each application's repository will need a new folder called `cluster-config` at the root. This is by convention to provide easier automation. Inside each of those folders are Kubernetes manifests specific to that application, such as additional `Service Accounts`, Istio `Gateways`, and `Deployments`.
