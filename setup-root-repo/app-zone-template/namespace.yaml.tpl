apiVersion: v1
kind: Namespace
metadata:
  name: "%%NAMESPACE%%"
  labels:
    created-by: root-configuration
  annotations:
    # Config Connector connection
    cnrm.cloud.google.com/project-id: %%PROJECT_ID%%