#!/bin/bash

# NOTE: This file is not necessary, could have run these commands in the install script. The purpose
# is to show that this is required as a first step (hince 01 prefix)

FILE="config-management-operator"

# Install Anthos based ACM
gsutil cp gs://config-management-release/released/latest/${FILE}.yaml ${FILE}.yaml
kubectl apply -f ${FILE}.yaml
rm -rf ${FILE}.yaml